package dz3.part2;

public class Visitor {
    private String nameVisitor; // Имя посетителя
    private int id; // Идентификатор посетителя

    private Book whichBookVisitorHasNow; // Какая книга сейчас на руках у посетителя

    private boolean firstTimeInTheLibrary = true; // Первый ли раз обращается посетитель за книгой

    private static int countVisitor; // Кол-во посетителей(необходимо для выдачи каждому посетителю своего индивидуального идентификатора). 1-ый посетитель будет с идентификатором 1, 2-ой посетитель с идентификатором 2 и т.д


    public Visitor() {
        countVisitor++;
    }

    /**
     * Конструктор объекта типа Visitor. В качестве аргумента будет подаваться только имя пользователя, так как идентификатор будет присваиваться
     * в методе одалживания при проверке посетителя на то, в какой раз он в библиотеке.
     * @param nameVisitor Имя посетителя
     */
    public Visitor(String nameVisitor) {
        this.nameVisitor = nameVisitor;
        countVisitor++;
    }

    /**
     * Метод возвращает имя посетителя
     * @return
     */
    public String getNameVisitor() {
        return nameVisitor;
    }

    /**
     * Метод устанавливает имя посетителя
     * @param nameVisitor Имя посетителя
     */
    public void setNameVisitor(String nameVisitor) {
        this.nameVisitor = nameVisitor;
    }

    /**
     * Метод возвращает идентификатор посетителя
     * @return
     */
    public int getId() {
        return id;
    }

    /**
     * Метод устанавливает идентификатор посетителя
     * @param id Идентификатор посетителя
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * Метод возвращает книгу, которая сейчас на руках у посетителя
     * @return
     */
    public Book getWhichBookVisitorHasNow() {
        return whichBookVisitorHasNow;
    }

    /**
     * Метод устанавливает какая книга будет у посетителя на руках
     * @param whichBookVisitorHasNow
     */
    public void setWhichBookVisitorHasNow(Book whichBookVisitorHasNow) {
        this.whichBookVisitorHasNow = whichBookVisitorHasNow;
    }

    /**
     * Метод возвращает true / false в зависимости какой раз посетитель в библиотеке
     * @return
     */
    public boolean getFirstTimeInTheLibrary() {
        return firstTimeInTheLibrary;
    }

    /**
     * Метод возвращает количество посетителей библиотеки(именно количество посетителей позволит нам присвоить каждому посетителю свой уникальный идентификатор)
     * @return
     */
    public static int getCountVisitor() {
        return countVisitor;
    }
}

